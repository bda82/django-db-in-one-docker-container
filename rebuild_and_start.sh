#!/bin/bash

set -e

echo "Stop Container if it is online..."

if [ $(docker ps -q -f name=done) ]; then 
	docker stop done
else 
	echo nope; 
fi

echo "Start Build Container..."

docker build -t done .

echo "Run Container..."

docker run \
		-detach \
		--rm \
		--name done \
		-p 5416:5432 \
		-p 8000:8000 \
		-p 8001:8001 \
		-p 8002:8002 \
		--env PGDATA=/var/lib/postgresql/pgdata \
		--volume /var/lib/done/data:/var/lib/postgresql/pgdata:rshared \
		--log-driver=journald \
		--log-opt tag=done \
		done:latest

echo "Complete..."

docker logs done --follow

# journalctl -b CONTAINER_TAG=done # logs from container with log-driver and log-opt