FROM postgres:11 

WORKDIR /var/lib/done

ENV PGDATA=/var/lib/done/data

ENV POSTGRES_USER=done
ENV POSTGRES_PASSWORD=done
ENV POSTGRES_DB=done
ENV POSTGRES_HOST=localhost
ENV POSTGRES_PORT=5432

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=.

RUN mkdir -p /var/lib/done 

RUN apt-get update
RUN apt install -y libpq-dev gcc make apt-transport-https ca-certificates build-essential

COPY create-pg-extensions.sql /docker-entrypoint-initdb.d/

VOLUME  ["/var/lib/postgresql"]

FROM python:3.7

WORKDIR /var/lib/done

COPY requirements.txt /var/lib/done/requirements.txt
COPY docker-entrypoint.sh /var/lib/done/docker-entrypoint.sh
COPY run_1_server.sh /var/lib/done/run_1_server.sh
COPY run_2_server.sh /var/lib/done/run_2_server.sh
COPY run_3_server.sh /var/lib/done/run_3_server.sh

RUN pip3 install wheel
RUN pip3 install -r requirements.txt

COPY ./src /var/lib/done

RUN env

RUN ls -la

RUN pwd

CMD ["postgres"]

ENTRYPOINT ["./docker-entrypoint.sh"]
