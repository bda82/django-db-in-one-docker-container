#!/bin/bash

set -e

run_1_server="echo run 1 server && \
  cd broadcast && \
  echo + manage makemigrations... && \
	python3 manage.py makemigrations --no-input && \
	echo + manage migrate... && \
	python3 manage.py migrate --no-input && \
	echo + runserver... && \
	python3 manage.py runserver 0.0.0.0:8000"

eval "${run_1_server}"