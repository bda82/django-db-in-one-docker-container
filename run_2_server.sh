#!/bin/bash

set -e

run_2_server="echo run Scheduler server && \
  cd scheduler && \
  echo + manage makemigrations... && \
	python3 manage.py makemigrations --no-input && \
	echo + manage migrate... && \
	python3 manage.py migrate --no-input && \
	echo + runserver... && \
	python3 manage.py runserver 0.0.0.0:8001"

eval "${run_2_server}"