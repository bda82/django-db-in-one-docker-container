#!/bin/bash

set -e

run_3_server="echo run Recommendation server && \
  cd recommendation && \
  echo + manage makemigrations... && \
	python3 manage.py makemigrations --no-input && \
	echo + manage migrate... && \
	python3 manage.py migrate --no-input && \
	echo + runserver... && \
	python3 manage.py runserver 0.0.0.0:8002"

eval "${run_3_server}"