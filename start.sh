#!/bin/sh

set -a

echo "Stop Container if it is online..."

docker stop done

echo "Run Container..."

docker run \
		-detach \
		--rm \
		--name done \
		-p 5416:5432 \
		--env PGDATA=/var/lib/postgresql/pgdata \
		--volume /var/lib/done/data:/var/lib/postgresql/pgdata:rshared \
		done:latest

echo "Complete..."

docker logs done --follow
